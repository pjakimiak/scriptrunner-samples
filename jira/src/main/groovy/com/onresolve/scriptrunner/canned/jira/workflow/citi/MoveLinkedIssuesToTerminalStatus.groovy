package com.onresolve.scriptrunner.canned.jira.workflow.citi

import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.status.Status
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.workflow.JiraWorkflow
import com.onresolve.scriptrunner.canned.CannedScript
import com.onresolve.scriptrunner.canned.util.BuiltinScriptErrors
import com.onresolve.scriptrunner.canned.util.SimpleBuiltinScriptErrors
import com.onresolve.scriptrunner.runner.customisers.ScriptListener
import com.opensymphony.workflow.loader.ActionDescriptor
import com.opensymphony.workflow.loader.StepDescriptor
import groovy.util.logging.Log4j

@ScriptListener
@Log4j
class MoveLinkedIssuesToTerminalStatus implements CannedScript {

    public static String FIELD_MY_PARAM = "FIELD_MY_PARAM"

    @Override
    String getName() {
        "Move Linked Issues to Terminal Status"
    }

    @Override
    String getDescription() {
        "Sample listener from a plugin XXX"
    }

    @Override
    List getCategories() {
        [] // unused
    }

    @Override
    List getParameters(Map params) {
        [
            [
                name       : FIELD_MY_PARAM,
                label      : "Some parameter",
                description: "Description of this parameter"
            ]
        ]
    }

    @Override
    BuiltinScriptErrors doValidate(Map<String, String> params, boolean forPreview) {
        def errors = new SimpleBuiltinScriptErrors()

        errors
    }

    @Override
    String getDescription(Map<String, String> params, boolean forPreview) {
        "preview description"
    }

    @Override
    Boolean isFinalParamsPage(Map params) {
        true // unused
    }

    @Override
    Map doScript(Map<String, Object> params) {
        log.warn("ScriptRunner listener: do something here")
        MutableIssue mainIssue = ComponentAccessor.getIssueManager().getIssueByCurrentKey(params.event.getIssue().getKey())
        def links = ComponentAccessor.getIssueLinkManager().getInwardLinks(mainIssue.id)
        def issueService = ComponentAccessor.getIssueService()
        links.each {

            def issue = it.sourceObject

            String actionName = "Opublikowano"

            JiraWorkflow workFlow = ComponentAccessor.getWorkflowManager().getWorkflow(issue)
            Status status = issue.getStatus()
            StepDescriptor currentStep = workFlow.getLinkedStep(status)
            List<ActionDescriptor> possibleActionsList = currentStep.getActions()

            log.error "Possible actions"
            possibleActionsList.each { log.error it }
            def action = possibleActionsList.find({ it.name.equalsIgnoreCase(actionName) })
            if (action == null) {
                return
            }

            IssueService.TransitionValidationResult transitionValidationResult = issueService.validateTransition(user, issue.getId(), action.id, issueService.newIssueInputParameters())
            if (transitionValidationResult.isValid()) {
                IssueService.IssueResult transitionResult = issueService.transition(user, transitionValidationResult)
            }
        }

        [:]
    }

    private ApplicationUser getUser() {
        ComponentAccessor.getUserManager().getUserByName("scriptrunner")
    }
}
