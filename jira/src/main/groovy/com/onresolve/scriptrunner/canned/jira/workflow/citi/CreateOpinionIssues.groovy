package com.onresolve.scriptrunner.canned.jira.workflow.citi


import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.bc.issue.link.RemoteIssueLinkService
import com.atlassian.jira.bc.project.component.ProjectComponent
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.component.pico.ComponentManager
import com.atlassian.jira.issue.IssueInputParameters
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.link.IssueLink
import com.atlassian.jira.issue.link.IssueLinkTypeManager
import com.atlassian.jira.issue.link.RemoteIssueLinkBuilder
import com.atlassian.jira.issue.status.Status
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.workflow.JiraWorkflow
import com.onresolve.scriptrunner.canned.CannedScript
import com.onresolve.scriptrunner.canned.util.BuiltinScriptErrors
import com.onresolve.scriptrunner.canned.util.SimpleBuiltinScriptErrors
import com.onresolve.scriptrunner.runner.customisers.ScriptListener
import com.opensymphony.workflow.loader.ActionDescriptor
import com.opensymphony.workflow.loader.StepDescriptor
import groovy.util.logging.Log4j


@ScriptListener
@Log4j
class CreateOpinionIssues implements CannedScript {

    public static String FIELD_MY_PARAM = "FIELD_MY_PARAM"

    @Override
    String getName() {
        "CreateOpinionIssues"
    }

    @Override
    String getDescription() {
        "Sample listener from a plugin"
    }

    @Override
    List getCategories() {
        [] // unused
    }

    @Override
    List getParameters(Map params) {
        [
                [
                        name       : FIELD_MY_PARAM,
                        label      : "Some parameter",
                        description: "Description of this parameter"
                ]
        ]
    }

    @Override
    BuiltinScriptErrors doValidate(Map<String, String> params, boolean forPreview) {
        def errors = new SimpleBuiltinScriptErrors()

        errors
    }

    @Override
    String getDescription(Map<String, String> params, boolean forPreview) {
        "preview description"
    }

    @Override
    Boolean isFinalParamsPage(Map params) {
        true // unused
    }

    @Override
    Map doScript(Map<String, Object> params) {

        MutableIssue mainIssue = ComponentAccessor.getIssueManager().getIssueByCurrentKey(params.event.getIssue().getKey())

        Map<Long, Long> linkedIssues = geLinkedIssuesWithProjects(mainIssue.id)

        for (ProjectComponent component : mainIssue.getComponents()) {
            def projectId = getProjectIdByComponent(component)
            if (linkedIssues.containsKey(projectId)) {
                processExistingOpinionTask(linkedIssues[projectId])
            } else {
                addNewOpinionTask(projectId, mainIssue)
            }
        }

        [:]
    }

    private def processExistingOpinionTask(Long issueId) {
        def issueService = ComponentAccessor.getIssueService()
        ApplicationUser user = getUser()
        MutableIssue issue = issueService.getIssue(user, issueId).issue

        String actionName = "Otwórz Ponownie"

        JiraWorkflow workFlow = ComponentAccessor.getWorkflowManager().getWorkflow(issue);
        Status status = issue.getStatus();
        StepDescriptor currentStep = workFlow.getLinkedStep(status);
        List<ActionDescriptor> possibleActionsList = currentStep.getActions();
        log.error "Possible actions"
        possibleActionsList.each { log.error it }
        def action = possibleActionsList.find({ it.name.equalsIgnoreCase(actionName) })
        if (action == null) {
            return
        }

        IssueService.TransitionValidationResult transitionValidationResult = issueService.validateTransition(user, issue.getId(), action.id, issueService.newIssueInputParameters());
        if (transitionValidationResult.isValid()) {
            IssueService.IssueResult transitionResult = issueService.transition(user, transitionValidationResult);
        }
    }

    private ApplicationUser getUser() {
        ComponentAccessor.getUserManager().getUserByName("scriptrunner")
    }


    private void addNewOpinionTask(Long targetProjectId, MutableIssue mainIssue) {
        IssueInputParameters issueInputParameters = ComponentAccessor.getIssueService().newIssueInputParameters()
        log.error("Reporter ${mainIssue.reporterId}")
        log.error("Assignee ${mainIssue.assigneeId}")
        issueInputParameters
                .setProjectId(targetProjectId)
                .setSummary("[Opiniowanie] ${mainIssue.summary}")
                .setDescription(mainIssue.description)
                .setIssueTypeId(getOpinionIssueTypeId())
                .setPriorityId(mainIssue.getPriority().getId())
                .setReporterId(getUser().key)
                .setAssigneeId(mainIssue.assigneeId)


        def opinionIssue = createIssue(issueInputParameters)
        linkIssues(mainIssue, opinionIssue)

        def remoteIssueLinkService = ComponentManager.instance.getComponentInstanceOfType(RemoteIssueLinkService.class)
        def mainIssueLinks = remoteIssueLinkService.getRemoteIssueLinksForIssue(getUser(), mainIssue)
        mainIssueLinks.remoteIssueLinks.each {
            def link = new RemoteIssueLinkBuilder(it).id(null).issueId(opinionIssue.getId()).build()
            def validationResult = remoteIssueLinkService.validateCreate(getUser(), link)
            if (validationResult.isValid()) {
                remoteIssueLinkService.create(getUser(), validationResult)
            } else {
                log.error("Unable to create remote link on issue ${opinionIssue.key}")
            }
        }

        issueInputParameters.setReporterId(mainIssue.reporterId)
        updateIssue(opinionIssue.getId(), issueInputParameters)
    }

    private def linkIssues(MutableIssue mainIssue, MutableIssue opinionIssue) {
        def linkTypeId = getLinkTypeId()
        log.error("link type id ${linkTypeId}")
        log.error("Main Issue ${mainIssue.key}")
        log.error("OP Issue ${opinionIssue.key}")
        ComponentAccessor.getIssueLinkManager().createIssueLink(opinionIssue.id, mainIssue.id, linkTypeId, null, getUser())
    }

    private String getOpinionIssueTypeId() {
        def opinionIssueType = ComponentAccessor.getConstantsManager().getRegularIssueTypeObjects().find({
            it.name.equalsIgnoreCase("opiniowanie")
        })
        return opinionIssueType != null ? opinionIssueType.id : null
    }

    private Map<Long, Long> geLinkedIssuesWithProjects(Long mainIssueId) {
        List<IssueLink> allOutIssueLink = ComponentAccessor.getIssueLinkManager().getInwardLinks(mainIssueId)
        def linkTypeId = getLinkTypeId()
        def linkedOpinionIssuesWithProjects = new LinkedHashMap()
        for (Iterator<IssueLink> outIterator = allOutIssueLink.iterator(); outIterator.hasNext();) {
            IssueLink issueLink = (IssueLink) outIterator.next()
            if (issueLink.getIssueLinkType().getId() == linkTypeId) {
                linkedOpinionIssuesWithProjects.put(issueLink.getSourceObject().getProjectObject().getId(),
                        issueLink.getSourceObject().getId())
            }
        }

        return linkedOpinionIssuesWithProjects
    }

    private Long getLinkTypeId() {
        def linkType = ComponentManager.instance.getComponentInstanceOfType(IssueLinkTypeManager.class).getIssueLinkTypes().find {
            it.inward.equalsIgnoreCase("jest uzupełniany przez")
        }
        log.error("link type ${linkType}")
        linkType != null ? linkType.id : null
    }

    private MutableIssue createIssue(IssueInputParameters issueInputParameters) {
        IssueService issueService = ComponentAccessor.getIssueService()
        ApplicationUser user = getUser()

        IssueService.CreateValidationResult createValidationResult =
                issueService.validateCreate(user, issueInputParameters)

        if (createValidationResult.isValid()) {
            IssueService.IssueResult createResult = issueService.create(
                    user, createValidationResult)

            if (!createResult.isValid()) {
                log.error("Failed to create issue" + createResult.errorCollection)
                throw new Exception("Failed to create issue" + createResult)
            }

            return createResult.issue
        }

        createValidationResult.errorCollection.errors.each { log.error it }
        throw new Exception("Failed to create issue - validation failed")
    }

    private MutableIssue updateIssue(Long issueId, IssueInputParameters issueInputParameters) {
        IssueService issueService = ComponentAccessor.getIssueService()
        ApplicationUser user = getUser()

        IssueService.UpdateValidationResult updateValidationResult =
                issueService.validateUpdate(user, issueId,issueInputParameters)

        if (updateValidationResult.isValid()) {
            IssueService.IssueResult updateResult = issueService.update(
                    user, updateValidationResult)

            if (!updateResult.isValid()) {
                log.error("Failed to create issue" + updateResult.errorCollection)
                throw new Exception("Failed to update issue" + updateResult)
            }

            return updateResult.issue
        }

        updateValidationResult.errorCollection.errors.each { log.error it }
        throw new Exception("Failed to create issue - validation failed")
    }

    private Long getProjectIdByComponent(ProjectComponent projectComponent) {
        HashMap<String, String> componentToProject = getComponentToProjectMap()
        def projectKey = componentToProject.get(projectComponent.getName())
        if (projectKey == null) {
            return null
        }
        def project = ComponentAccessor.getProjectManager().getProjectByCurrentKey(projectKey)
        return project.getId()
    }

    private HashMap<String, String> getComponentToProjectMap() {
        def componentToProject =
                [
                        "Bankowość Detaliczna"  : "JBKDT",
                        "Prawny"                : "JPRAW",
                        "Operacje i Technologie": "JOPTE",
                ]
        return componentToProject
    }
}


