package com.onresolve.scriptrunner.canned.jira.workflow.citi

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.user.ApplicationUser
import com.onresolve.scriptrunner.canned.CannedScript
import com.onresolve.scriptrunner.canned.util.BuiltinScriptErrors
import com.onresolve.scriptrunner.canned.util.SimpleBuiltinScriptErrors
import com.onresolve.scriptrunner.runner.customisers.ScriptListener
import groovy.util.logging.Log4j

@ScriptListener
@Log4j
class FillDateField implements CannedScript {

    public static String FIELD_MY_PARAM = "FIELD_MY_PARAM"

    @Override
    String getName() {
        "FillDataField"
    }

    @Override
    String getDescription() {
        "Sample listener from a plugin XXX"
    }

    @Override
    List getCategories() {
        [] // unused
    }

    @Override
    List getParameters(Map params) {
        [
                [
                        name       : FIELD_MY_PARAM,
                        label      : "Some parameter",
                        description: "Description of this parameter"
                ]
        ]
    }

    @Override
    BuiltinScriptErrors doValidate(Map<String, String> params, boolean forPreview) {
        def errors = new SimpleBuiltinScriptErrors()

        errors
    }

    @Override
    Map doScript(Map<String, Object> params) {
        log.warn("ScriptRunner listener: do something here")

        MutableIssue issue = ComponentAccessor.getIssueManager().getIssueByCurrentKey(params.event.getIssue().getKey())
//        Data Podpisania
//        Data Akceptacji Szefa Jednostki
//        Data Akceptacji Szefa Pionu
//        Data Akceptacji Szefa Biura Organizacyjnego


        def fieldToSet = getFieldIdFromName("Data Podpisania")
        mainIssue.setCustomFieldValue(fieldToSet, new java.sql.Date(new java.util.Date().getTime()))
        ComponentAccessor.getIssueManager().updateIssue(getUser(), mainIssue, EventDispatchOption.ISSUE_UPDATED, false)

        [:]
    }

    @Override
    String getDescription(Map<String, String> params, boolean forPreview) {
        "preview description"
    }

    @Override
    Boolean isFinalParamsPage(Map params) {
        true // unused
    }


    CustomField getFieldIdFromName(fieldName) {
        def customField = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(fieldName)
        customField != null ? customField : null
    }

    private ApplicationUser getUser() {
        ComponentAccessor.getUserManager().getUserByName("scriptrunner")
    }


}


