package com.onresolve.scriptrunner.canned.confluence.macros

import com.atlassian.applinks.api.ApplicationLinkService
import com.atlassian.applinks.api.application.jira.JiraApplicationType
import com.atlassian.sal.api.component.ComponentLocator
import com.atlassian.sal.api.net.ResponseException
import com.atlassian.sal.api.net.ResponseHandler
import com.onresolve.scriptrunner.runner.rest.common.CustomEndpointDelegate
import groovy.json.JsonBuilder
import groovy.transform.BaseScript
import org.codehaus.jackson.map.ObjectMapper

import javax.servlet.http.HttpServletRequest
import javax.ws.rs.core.MultivaluedMap
import javax.ws.rs.core.Response

import static com.atlassian.sal.api.net.Request.MethodType.POST

@BaseScript CustomEndpointDelegate delegate

createIssueEndpoint(
        httpMethod: "POST", groups: ["confluence-administrators"]
) { MultivaluedMap queryParams, String body, HttpServletRequest req ->

    log.error("createIssueEndpoint")
    log.error(body)

    def mapper = new ObjectMapper()
    def data = mapper.readValue(body, Map)

    ApplicationLinkService appLinkService = ComponentLocator.getComponent(ApplicationLinkService)
    def appLink = appLinkService.getPrimaryApplicationLink(JiraApplicationType)
    def applicationLinkRequestFactory = appLink.createAuthenticatedRequestFactory()

    def issuePayload = new JsonBuilder([
            fields: [
                    project    : [key: "AN"],
                    summary    : data.summary,
                    description: "Build and deploy a release",
                    issuetype  : [name: "AN from Template "]
            ]
    ]).toString()

    def request = applicationLinkRequestFactory.createRequest(POST, "/rest/api/2/issue")
            .addHeader("Content-Type", "application/json")
            .setEntity(issuePayload)

    request.execute(new ResponseHandler<com.atlassian.sal.api.net.Response>() {
        @Override
        void handle(com.atlassian.sal.api.net.Response response) throws ResponseException {
            if (response.statusCode != 201) {
                log.error("Creating Jira issue failed: ${response.responseBodyAsString}")
            }
        }
    })

    return Response.ok("Received data:  $data").build()
}