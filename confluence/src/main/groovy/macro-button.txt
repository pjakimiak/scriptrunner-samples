"""<button id='create'>Create</button>
   <input type="hidden" id='page-title' value='${context.pageContext.pageTitle}'></input>
   <input type="hidden" id='target-project' value='${parameters.targetProject}'></input>
"""



AJS.toInit(function() {
  let buttonSelector = 'button#create';
  let button = document.querySelector(buttonSelector);
  AJS.$('#main-content').find('button').addClass('aui-button aui-button-primary');


  let f = function clickHandler(clickEvent) {
    let pageTitle = document.querySelector("input#page-title").value;

    AJS.$.ajax({
        beforeSend: function (xhr) {
        xhr.setRequestHeader("X-Atlassian-Token", "no-check");
        xhr.setRequestHeader(  "Content-Type", "application/json");
    },
      type: "POST",
      dataType: 'json',
      url : AJS.params.contextPath + "/rest/scriptrunner/latest/custom/createIssueEndpoint",
          data: '{"summary": "'+pageTitle+'"}'
    }).done(function (data, status, jqXhr) {
      console.log("Retrieved response from custom rest endpoint: ");
      console.log(status);
    });;
  };

  button.addEventListener("click", f);

})




